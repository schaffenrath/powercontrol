#pragma once
#include <string>
#include <ctime>
#include <iostream>

#ifdef __arm__
#include <wiringPi.h>
#include <lcd.h>
#endif

#include "control.hpp"

enum LCD_PINS {VDD = 0, LED_POSITIVE = 1, RS = 2, E = 3, DB0 = 4, DB1 = 5, DB2 = 6, DB3 = 7, DB4 = 8, DB5 = 9, DB6 = 10, DB7 = 11}; 
enum LCD_LINE_MODE {TEXT = 0, DATE_TIME = 1, CALENDAR = 2, NEWS = 3};

void control_lcd(general_control_t *control);
