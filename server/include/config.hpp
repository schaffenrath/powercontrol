#pragma once
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <fstream>
#include <iostream>
#include <string>

#include "control.hpp"

#define CONFIG_FILE "powercontrol_config.json"

using boost::property_tree::ptree;
using boost::property_tree::read_json;
using boost::property_tree::write_json;
using namespace std;

enum Config_errors {
  CONFIG_SUCCESS = 0,
  OPEN_FAIL = -1,
  WRITE_FAIL = -2,
  WRONG_ATTRIBUTE = -3,
  INVALID_SYNTAX = -4
};

int load_config(general_control_t *control);
int save_config(const general_control_t *control);
