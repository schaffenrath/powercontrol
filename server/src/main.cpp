#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <string>
#include <vector>

#ifdef __arm__
#include <softPwm.h>
#include <wiringPi.h>
#endif

#include "../include/config.hpp"
#include "../include/control.hpp"
#include "../include/led.hpp"
#include "../include/network.hpp"
#include "../include/lcd.hpp"
using namespace std;

int main(int argc, char **argv) {
  general_control_t control;
  load_config(&control);
  if (wiringPiSetupGpio() == -1) return EXIT_FAILURE;
  boost::thread control_socket_thread(control_socket, &control);
  boost::thread control_led_thread(control_led, &control);
  boost::thread control_lcd_thread(control_lcd, &control);

  std::unique_lock<mutex> shutdown_lock(control.mtx);
  while (!control.shutdown)
    control.shutdown_var.wait(shutdown_lock);

  control_socket_thread.interrupt();
  control_led_thread.join();
  control_lcd_thread.join();
  return EXIT_SUCCESS;
}
