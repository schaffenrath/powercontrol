package com.robert.testfragment;
import com.robert.testfragment.Network;

public class LedColor {
    private int red;
    private int green;
    private int blue;
    private int alpha;

    public LedColor() {
        setColor(0,0,0,0);
    }

    public LedColor(int red, int green, int blue) {
        setColor(red, green, blue);
    }

    public LedColor(int alpha, int red, int green, int blue) {
        setColor(alpha, red, green, blue);
    }

    public int getAlpha() {
        return alpha;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public void setColor(int red, int green, int blue) {
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public void setColor(int alpha, int red, int green, int blue) {
        this.alpha = alpha;
        setColor(red, green, blue);
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public String getChangeColorNetCommand() {
        return new String(Network.COMMAND_LED_COLOR + Network.COMMAND_DELIMITER + alpha + Network.VALUE_DELIMITER + red + Network.VALUE_DELIMITER + green + Network.VALUE_DELIMITER + blue + Network.COMMAND_DELIMITER + Network.MESSAGE_TERMINATOR);
    }
}
