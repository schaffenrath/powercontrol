#include "../include/relai.hpp"

bool create_relai(general_control_t *control, const string &name, const int pin) {
	if (control->pin_layout.enable_pin[pin])
		return false;
	control->pin_layout.enable_pin[pin] = true;
	control->pin_layout.pin_mode[pin] = RELAI;
	relai_control_t new_relai;
	new_relai.pin = pin;
	new_relai.state = false;
	control->relai_control_list.insert(pair<string,relai_control_t>(name, std::move(new_relai)));
	control->relai_change_var.notify_all();
	return true;
}

bool create_relai_group(general_control_t *control, const string &group_name, vector<string> &relai_names) {
	relai_group_t group;
	group.state = false;
	for (string relai_name : relai_names) {
		auto relai = control->relai_control_list.find(relai_name);
		if (relai == control->relai_control_list.end())
			return false;
		group.members.push_back(relai_name);
		control->relai_control_list[relai_name].group_member.push_back(group_name);
	}
	control->relai_group_list.insert(pair<string, relai_group_t>(group_name, move(group)));
	return true;	
}

void remove_relai(general_control_t *control, const string &name) {
	for (string &member_group : control->relai_control_list[name].group_member) {
		for (auto it = control->relai_group_list[member_group].members.begin(); it != control->relai_group_list[member_group].members.end(); it++) {
			if (it->compare(name) == 0) {
				control->relai_group_list[member_group].members.erase(it);
				break;
			}
		}
		control->relai_group_list[member_group].members;
	}
	control->pin_layout.enable_pin[control->relai_control_list[name].pin] = false;
	control->relai_control_list.erase(name);
}

void remove_relai_group(general_control_t *control, const string &group_name) {
	for (string &member : control->relai_group_list[group_name].members) {
		for (auto it = control->relai_control_list[member].group_member.begin(); it != control->relai_control_list[member].group_member.end(); it++) {
			if (it->compare(group_name) == 0) {
				control->relai_control_list[member].group_member.erase(it);
				break;
			}
		}
	}
	control->relai_group_list.erase(group_name);
}

bool switch_relai(general_control_t *control, const string &group_name, const bool state) {
	auto relai = control->relai_control_list.find(group_name);
	if (relai == control->relai_control_list.end())
		return false;
	relai->second.state = state;
	control->relai_change_var.notify_all();
	return true;
}

bool switch_relai_group(general_control_t *control, const string &group_name, const bool state){
	for (string relai : control->relai_group_list[group_name].members) {
		if (control->relai_control_list[relai].group_member.size() > 1 && state == false) {
			bool all_off = true;
			for (string group : control->relai_control_list[relai].group_member) {
				if (control->relai_group_list[group].state == true){
					all_off = false;
					break;
				}
			}
			if (all_off) {
				control->relai_control_list[relai].state = state;
			}
		}
		else  {
			control->relai_control_list[relai].state = state;
		}
	}
	control->relai_change_var.notify_all();
	return true;
}

void control_relai(general_control_t *control) {
	for (auto relai : control->relai_control_list) 
		digitalWrite(relai.second.pin, relai.second.state ? LOW : HIGH);

	unique_lock<mutex> disable_lock(control->mtx);
	
	while(!control->shutdown) {
		while(!control->relai_change)
			control->relai_change_var.wait(disable_lock);

	for (auto relai : control->relai_control_list) 
		digitalWrite(relai.second.pin, relai.second.state ? LOW : HIGH);
	}
	
}
