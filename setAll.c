#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>
#include <string.h>

int main(int argc, char **argv) {

	int relayState = 0;

	if (argc == 1) {
		printf("No argument entered!\nPlease enter 'on'/'1' to turn all relays on or 'off'/'0' to turn all off.\n");
		return EXIT_FAILURE;
	}
	if (argc > 2) {
		printf("Too many arguments!\nPlease enter 'on'/'1' to turn all relays on or 'off'/'0' to turn all off.\n");
	}

	if (strcmp(argv[1], "on") == 0 || strcmp(argv[1], "1") == 0)
		relayState = 1;

	else if (strcmp(argv[1], "off") == 0 || strcmp(argv[1], "0") == 0)
		relayState = 0;

	else {
		printf("Entered wrong argument!Please enter 'on'/'1' to turn all relays on or 'off'/'0' to turn all off.\n");
	}

	if (wiringPiSetupGpio() == -1)
			return EXIT_FAILURE;

	for (unsigned int i=0; i < 28; i++) {
		pinMode(i, OUTPUT);
		digitalWrite(i, relayState);
	}
	
	return 0;
}


