#pragma once
#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread.hpp>
#include <condition_variable>
#include <iostream>
#include <mutex>
#include <sstream>

#include "control.hpp"
#include "led.hpp"
#include "lcd.hpp"

using namespace std;
using boost::asio::ip::tcp;

#define CONTROL_PORT 65234

enum Network_command {
  SHUTDOWN_SIGNAL					 			=	   0,
  DISCONNECT_SIGNAL				 			=		 1,
	VERIFY_CONNECTION							=		 2,
	REQUEST_PINS									=	  10,
	REQUEST_LED_STATE	 						=   11,
	REQUEST_LED_COLOR	 						=   12,
	REQUEST_LED_EFFECT  					=   13,
	REQUEST_RELAI_GROUPS					=		14,		
	REQUEST_RELAI_STATE 					=   15,
	REQUEST_RELAI_TIMER 					=   16,
	REQUEST_LCD_STATE	 						=   17,
	REQUEST_LCD_MODE		 					=	  18,
	REQUEST_LCD_TEXT		 					=	  19,
	COMMAND_CREATE_LED_GROUP 			=	 100,
  COMMAND_LED_STATE				 			=  101,
  COMMAND_LED_COLOR				 			=  102,
  COMMAND_LED_EFFECT			 			=  103, 
	COMMAND_CREATE_RELAI					=	 104, 
	COMMAND_CREATE_RELAI_GROUP 		=  105, 
	COMMAND_RELAI_STATE			 			=  106,
	COMMAND_RELAI_TIMER			 			=  107,
	COMMAND_REMOVE_RELAI					= 108,
	COMMAND_REMOVE_RELAI_GROUP				= 109,
	COMMAND_LCD_STATE				 			=	 109,
	COMMAND_LCD_MODE				 			=  110,
	COMMAND_LCD_TEXT				 			=	 111
};

enum Network_error {
  COMMAND_EXECUTED = 0,
  ERROR = -1,
  INVALID_ARGUMENTS = -2,
  LED_GROUP_NOT_FOUND = -5,
  LED_PIN_NOT_CONFIGURED = -6
};

#define VALUE_DELIMITER ':'
#define COMMAND_DELIMITER '\t'
#define MESSAGE_TERMINATOR '\n'

void control_socket(general_control_t *control);
