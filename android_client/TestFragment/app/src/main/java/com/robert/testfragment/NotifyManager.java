package com.robert.testfragment;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.app.NotificationCompat;
import androidx.core.app.RemoteInput;

public class NotifyManager {
    NotificationManager manager;
    NotificationCompat.Builder builder;
    NotificationChannel channel;
    Context context;
    Intent intent;
    Intent replyIntent;
    PendingIntent pendingIntent;

    final String CHANNEL_ID = "1";
    final String CHANNEL_NAME = "Powercontrol";

    public NotifyManager(Context context) {
        this.context = context;
        channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
        channel.setShowBadge(true);
        channel.enableVibration(true);
        channel.setVibrationPattern(new long[] {100, 100});
        channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);

        manager = context.getSystemService(NotificationManager.class);
        manager.createNotificationChannel(channel);
        intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
    }

    public void createNotification(String title, String body) {
        RemoteInput remoteInput = new RemoteInput.Builder("key_text_reply")
                .setLabel("Set IP")
                .build();

        replyIntent = new Intent(context, NotifyManager.class);
        PendingIntent replyPendingIntent = PendingIntent.getBroadcast(context, 0, replyIntent, 0);
        NotificationCompat.Action action = new NotificationCompat.Action.Builder(R.drawable.ic_brush, "Enter IP", replyPendingIntent)
                .addRemoteInput(remoteInput)
                .build();


        builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_powerblack)
                .setContentTitle(title)
                .setContentText(body)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setContentIntent(pendingIntent)
                .addAction(action)
                .setAutoCancel(true);
        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(1, builder.build());
    }

    public String getNotificationMessage() {
        Bundle remoteMessage = RemoteInput.getResultsFromIntent(replyIntent);
        if (remoteMessage != null) {
            CharSequence receiveText = remoteMessage.getCharSequence("key_text_reply");
            if (receiveText != null)
                return receiveText.toString();
            else
                return null;
        }
        else
            return null;
    }
}
