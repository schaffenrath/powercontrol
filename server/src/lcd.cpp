#include "../include/lcd.hpp"
#include <boost/thread.hpp>
#include <condition_variable>
#include <boost/shared_ptr.hpp>
#include <mutex>

const string get_month_name(const int month_nr) {
    switch (month_nr) {
        case 0: return "Jan";
        case 1: return "Feb";
        case 2: return "Mar";
        case 3: return "Apr";
        case 4: return "Mai";
        case 5: return "Jun";
        case 6: return "Jul";
        case 7: return "Aug";
        case 8: return "Sep";
        case 9: return "Oct";
        case 10: return "Nov";
        case 11: return "Dec";
        default: return "Err";
    }
}

inline string get_two_digit(int value) {
    return value < 10 ? ("0"+to_string(value)) : to_string(value);
}

void display_time(general_control_t *control, const int display, const int line, boost::shared_ptr<recursive_mutex> time_lock) {
    time_t now = time(0);
    tm *time_struct = localtime(&now);
    time_lock->lock();
    lcdPosition(display, 0, line);
    lcdPuts(display, (get_two_digit(time_struct->tm_mday) + "." + get_month_name(time_struct->tm_mon)).c_str());
    time_lock->unlock();

    while (!control->lcd_control.line_change[line]) {
			now = time(0);
			time_struct = localtime(&now);
			
			if (time_struct->tm_hour == 0 && time_struct->tm_min == 0) {
					time_lock->lock();
					lcdPosition(display, 0, line);
					lcdPuts(display, (get_two_digit(time_struct->tm_mday) + "." + get_month_name(time_struct->tm_mon)).c_str());
					time_lock->unlock();
			}
			
				time_lock->lock();
				lcdPosition(display, 8, line);
				lcdPuts(display, (get_two_digit(1+time_struct->tm_hour) + ":" + get_two_digit(time_struct->tm_min) + ":" + get_two_digit(time_struct->tm_sec)).c_str());
				time_lock->unlock();
				delay(1000);
    }
		control->lcd_control.line_change[line] = false;
		time_lock->lock();
		lcdPosition(display, 0, line);
		lcdPuts(display, "                ");
		time_lock->unlock();
}

void display_text( general_control_t *control, const int display, const int line, boost::shared_ptr<recursive_mutex> text_lock) {
    size_t text_length = control->lcd_control.line_text[line].size();
    size_t shift = 0;

    while (!control->lcd_control.line_change[line]) {
			if (text_length > control->lcd_control.display_length) {
					text_lock->lock();
					lcdPosition(display, 0, line);
					lcdPuts(display, control->lcd_control.line_text[line].substr(shift, control->lcd_control.display_length).c_str());
					text_lock->unlock();
					delay(500);
					shift ++;
					if (shift > (text_length - (control->lcd_control.display_length))) {
							shift = 0;
							delay(800);
					}
        } else {
					text_lock->lock();
					lcdPosition(display, 0, line);
					lcdPuts(display, control->lcd_control.line_text[line].c_str());
					text_lock->unlock();
					delay(1000);
        }
    }
		control->lcd_control.line_change[line] = false;
		text_lock->lock();
		lcdPosition(display, 0, line);
		lcdPuts(display, "                ");
		text_lock->unlock();
}

void control_line(general_control_t *control, const int display, const int line, boost::shared_ptr<recursive_mutex> display_mutex) {
    while (!control->shutdown) {
        switch (control->lcd_control.line_mode[line]) {
            case TEXT:
                display_text(control, display, line, display_mutex);
                break;
            case DATE_TIME:
                display_time(control, display, line, display_mutex);
                break;
            default: 
                cerr<<"Wrong LCD mode!"<<endl;
                break;
        }
    }
}

void control_lcd(general_control_t *control) {
  int display = lcdInit(2, 16, 4, control->lcd_control.lcd_pins[RS],control->lcd_control.lcd_pins[E], control->lcd_control.lcd_pins[DB0], control->lcd_control.lcd_pins[DB1], control->lcd_control.lcd_pins[DB2], control->lcd_control.lcd_pins[DB3], 0, 0, 0, 0);
	lcdClear(display);

	boost::shared_ptr<recursive_mutex> display_mutex = boost::make_shared<recursive_mutex>();
	boost::thread line1_thread(control_line, control, display, 0, display_mutex);
	boost::thread line2_thread(control_line, control, display, 1, display_mutex);

}
