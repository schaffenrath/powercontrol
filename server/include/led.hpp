#pragma once

#ifdef __arm__
#include <softPwm.h>
#include <wiringPi.h>
#endif

#include "control.hpp"

enum Led_color { ALPHA = 0, RED = 1, GREEN = 2, BLUE = 3, WHITE = 4 };
enum Led_mode { constant, fade_slow, fade_fast, change_slow, change_quick };

float get_internal_color(int color_value, int alpha);
int get_external_color(float color_value, int alpha);
bool control_led(general_control_t *control);
bool switch_led(general_control_t *control, const bool state);
void change_led_color(general_control_t *control, int red, int green, int blue,
                      int alpha);
