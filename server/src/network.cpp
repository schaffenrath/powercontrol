#include "../include/network.hpp"
#include "../include/config.hpp"
#include "../include/relai.hpp"


int parse_led_command(general_control_t *control, string message) {
  string color_value, substring;
  size_t seperator_pos = 0, prev_pos = 0;

  seperator_pos = message.find(VALUE_DELIMITER);
  prev_pos = 0;

  const int led_nr = control->led_control.is_rgbw ? RGBW_LED_NR : RGB_LED_NR;
		float alpha = 0.0;

  for (int i = 0; i < led_nr + 1; i++) {
    substring = message.substr(prev_pos, message.size());
    seperator_pos = substring.find(VALUE_DELIMITER);

    if (seperator_pos == string::npos) {
      seperator_pos = substring.find(COMMAND_DELIMITER);

      if (seperator_pos == string::npos) {
        cerr << "Received wrong number of arguments for led control!" << endl;
        return INVALID_ARGUMENTS;
      }
    }

    color_value = message.substr(prev_pos, seperator_pos);

    switch (i) {
    case ALPHA:
			alpha = (float)stoi(color_value);
      control->led_control.color_led[ALPHA] = alpha;
      break;
    case RED:
      control->led_control.color_led[RED] = get_internal_color(stoi(color_value), alpha);
      break;
    case GREEN:
      control->led_control.color_led[GREEN] = get_internal_color(stoi(color_value), alpha);
      break;
    case BLUE:
      control->led_control.color_led[BLUE] = get_internal_color(stoi(color_value), alpha);
      break;
    case WHITE:
      control->led_control.color_led[WHITE] = get_internal_color(stoi(color_value), alpha);
      break;
    default: 
      cerr<<"Error in setting color value from network. This should never be reached!"<<endl;
      return INVALID_ARGUMENTS;
    }
    prev_pos += seperator_pos + 1;
  }
  return COMMAND_EXECUTED;
}

void control_handle(boost::shared_ptr<tcp::socket> socket, tcp::acceptor *acceptor, 
                    general_control_t *control) {
  try {
    bool active_connection = true;
    while (active_connection) {
        string message, network_command, server_answer;
        size_t delimiter_pos = 0;
        boost::asio::streambuf message_buf;
        try{
          boost::asio::read_until(*socket, message_buf, MESSAGE_TERMINATOR);
        } catch (exception &ex) {
          active_connection = false;
          break;
        }
        istream message_stream(&message_buf);
        getline(message_stream, message);
        delimiter_pos = message.find(COMMAND_DELIMITER);
        network_command = message.substr(0, delimiter_pos);
        message = message.substr(delimiter_pos+1, message.size());
				server_answer.clear();

      switch (stoi(network_command)) {
      case SHUTDOWN_SIGNAL: 
        control->shutdown = true;
        control->led_enable_var.notify_all();
        control->shutdown_var.notify_all();
        active_connection = false;

        server_answer = to_string(COMMAND_EXECUTED) + COMMAND_DELIMITER;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
       break;

      case DISCONNECT_SIGNAL:
        active_connection = false;
        server_answer = to_string(COMMAND_EXECUTED) + COMMAND_DELIMITER;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
        break;

			case VERIFY_CONNECTION:
				server_answer = to_string(COMMAND_EXECUTED) + MESSAGE_TERMINATOR;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
				break;

			case REQUEST_PINS: 
				server_answer.clear();
				server_answer = to_string(0) + VALUE_DELIMITER + to_string(control->pin_layout.enable_pin[0]) + VALUE_DELIMITER + to_string(control->pin_layout.pin_mode[0]) + COMMAND_DELIMITER;
				
				for (size_t i=1; i < PIN_NR; i++) 
					server_answer += to_string(i) + VALUE_DELIMITER + to_string(control->pin_layout.enable_pin[i]) + VALUE_DELIMITER + to_string(control->pin_layout.pin_mode[i]) + COMMAND_DELIMITER;

				server_answer += MESSAGE_TERMINATOR;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, server_answer.length()));
				break;

			case REQUEST_LED_STATE:
				server_answer = to_string(control->led_control.enable) + MESSAGE_TERMINATOR;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, server_answer.length()));
				break;
				
			case REQUEST_LED_COLOR: {
																		 const int alpha = control->led_control.alpha; 
			  server_answer = to_string(alpha) + VALUE_DELIMITER + to_string(get_external_color(control->led_control.red, alpha)) + VALUE_DELIMITER + to_string(get_external_color(control->led_control.green, alpha)) + VALUE_DELIMITER + to_string(get_external_color(control->led_control.blue, alpha)) + COMMAND_DELIMITER + MESSAGE_TERMINATOR;
				for (size_t i=0; i < RGB_LED_NR; i++)
					server_answer += to_string(get_external_color(control->led_control.color_led[i], alpha)) + (i==RGB_LED_NR-1?VALUE_DELIMITER:COMMAND_DELIMITER);
        boost::asio::write(*socket, boost::asio::buffer(server_answer, server_answer.length()));
				}
				break;

			case REQUEST_RELAI_GROUPS:
				server_answer = "";
				for (auto relai : control->relai_control_list) {
					server_answer += relai.first + VALUE_DELIMITER;
					server_answer += to_string(relai.second.state) + VALUE_DELIMITER;
				}
        boost::asio::write(*socket, boost::asio::buffer(server_answer, server_answer.length()));
				break;
				
      case COMMAND_LED_STATE: {
        delimiter_pos = message.find(COMMAND_DELIMITER);
        bool state =
            stoi(message.substr(delimiter_pos + 1)) == 0 ? false : true;
        switch_led(control, state);
        save_config(control);
        server_answer = to_string(control->led_control.enable) + MESSAGE_TERMINATOR;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
      } break;

      case COMMAND_LED_COLOR: {
        int parse_return = 0;
        if ((parse_return = parse_led_command(control, message)) == COMMAND_EXECUTED)
          save_config(control);
        server_answer = to_string(parse_return) + MESSAGE_TERMINATOR;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
      } break;

			case COMMAND_CREATE_RELAI: {
				delimiter_pos = message.find(VALUE_DELIMITER);
				string name = message.substr(0, delimiter_pos);
				message = message.substr(delimiter_pos+1, message.size());
				delimiter_pos = message.find(VALUE_DELIMITER);
				int pin = stoi(message.substr(0, delimiter_pos));
				bool successCreated = create_relai(control, name, pin);
				server_answer = to_string(successCreated ? COMMAND_EXECUTED : ERROR) + MESSAGE_TERMINATOR;
				boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
        save_config(control);
			} break;

      case COMMAND_CREATE_RELAI_GROUP:
      {
        delimiter_pos = message.find(VALUE_DELIMITER);
        string group_name = message.substr(0,delimiter_pos);
        message = message.substr(delimiter_pos+1, message.size());
        vector<string> group_members;
        delimiter_pos = message.find(VALUE_DELIMITER);
        const char delimiter_const[] = {COMMAND_DELIMITER};
        while (message.substr(0, 1).compare(delimiter_const) != 0) {
          group_members.push_back(message.substr(0, delimiter_pos));
          message = message.substr(delimiter_pos+1, message.size());
        }
        create_relai_group(control, group_name, group_members);
      }
      break;

      case COMMAND_RELAI_STATE:
        server_answer = to_string(COMMAND_EXECUTED) + COMMAND_DELIMITER;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
        break;

			case COMMAND_LCD_TEXT: {
				delimiter_pos = message.find(COMMAND_DELIMITER);
				string text = message.substr(0, delimiter_pos);
				control->lcd_control.line_text[0] = text;
				control->lcd_control.line_change[0] = true;
        server_answer = to_string(COMMAND_EXECUTED) + COMMAND_DELIMITER;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
				}
				break;

      default:
        cerr << "Received wrong command: \""<<message<<"\" from IP:"
             << socket->remote_endpoint().address().to_string() << endl;
        server_answer = to_string(ERROR) + COMMAND_DELIMITER;
        boost::asio::write(*socket, boost::asio::buffer(server_answer, sizeof(server_answer)));
        break;
      }
    }
    socket->close();
  } catch (exception &e) {
    cerr << e.what() << endl;
  }
}

void control_socket(general_control_t *control) {
  try {
  boost::asio::io_service io_service;
    tcp::acceptor acceptor(io_service, tcp::endpoint(tcp::v4(), CONTROL_PORT));
    while (!control->shutdown) {
      boost::shared_ptr<tcp::socket> socket =
          boost::make_shared<tcp::socket>(tcp::socket(io_service));
      acceptor.listen();
      acceptor.accept(*socket);

      boost::thread server_thread(control_handle, socket, &acceptor, control);
    }
  } catch (exception &e) {
    cerr << e.what() << endl;
  }
}
