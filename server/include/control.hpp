#pragma once
#include <boost/asio.hpp>
#include <ctime>
#include <memory>
#include <string>
#include <vector>
#include <map>

#define PIN_NR 28
#define RGB_LED_NR 3
#define RGBW_LED_NR 4

using namespace std;

enum Pin_enable { ENABLED = true, DISABLED = false };
enum Led_effect {
  STATIC = 0,
  FADE_SLOW = 1,
  FADE_MIDDLE = 2,
  FADE_FAST = 4,
  JUMP_SLOW = 5,
  JUMP_MIDDLE = 6,
  JUMP_FAST = 7
};
enum Pin_mode {
  NOT_DEFINED = 0,
  IN = 1,
  OUT = 2,
  LED = 3,
  RELAI = 4,
  DISPLAY = 5
};

typedef struct {
  bool enable_pin[PIN_NR];
  int pin_mode[PIN_NR];
} pin_layout_t;

typedef struct {
  bool enable = false;
  bool is_rgbw = false;
  int color_pin[RGBW_LED_NR+1];
	float color_led[RGBW_LED_NR+1];
  float red = 0;
  float green = 0;
  float blue = 0;
  float white = 0;
  float alpha = 0;
  int effect_mode;
} led_control_t;

typedef struct {
  string switch_name;
  time_t time_on;
  time_t time_off;
  int remaining_time;
  bool switch_to_state;
} relai_time_control_t;

typedef struct {
	int pin;
  bool state;
	vector<string> group_member;
  vector<relai_time_control_t> time_control;
} relai_control_t;

typedef struct {
	bool state;
	vector<string> members;
} relai_group_t;

typedef struct {
  bool enable = true;
	int lcd_pins[12];
  int line_mode[2];
  string line_text[2];
  bool line_change[2] = {false, false};
  size_t display_length;
  size_t display_rows;
} lcd_control_t;

typedef struct {
  bool shutdown = false;
  mutex mtx;
  condition_variable shutdown_var;
  condition_variable led_enable_var;
	condition_variable relai_change_var;
  pin_layout_t pin_layout;
  led_control_t led_control;
	bool relai_change = false;
  map<string, relai_control_t> relai_control_list;
	map<string, relai_group_t> relai_group_list;
  lcd_control_t lcd_control;
} general_control_t;
