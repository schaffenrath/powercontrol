#include "../include/config.hpp"
#include "../include/led.hpp"
#include "../include/lcd.hpp"
#include <boost/foreach.hpp>
#include <memory>

int load_config(general_control_t *control) {
    ptree root;

    try {
        read_json(CONFIG_FILE, root); 
    } catch (boost::property_tree::json_parser::json_parser_error &er) {
        for (int i=0; i < PIN_NR; i++) {
            control->pin_layout.enable_pin[i] = false;
            control->pin_layout.pin_mode[i] = NOT_DEFINED;
        }
        return OPEN_FAIL;
    }
    
    string config_category;
    string attribute;
    string value;

    try {
			BOOST_FOREACH (ptree::value_type &pin_group, root.get_child("pin_layout")) {
					const int pin = pin_group.second.get<int>("pin");
					control->pin_layout.enable_pin[pin] = pin_group.second.get<bool>("enable");
					control->pin_layout.pin_mode[pin] = pin_group.second.get<int>("mode");
			}
    } catch (exception &ex) {

    }

    try {
					auto led_attr = root.get_child("led_control");
					control->led_control.enable = led_attr.get<bool>("enable");
					control->led_control.is_rgbw = led_attr.get<bool>("is_rgbw");
					control->led_control.color_pin[RED] = led_attr.get<int>("pin_red");
					control->led_control.color_pin[GREEN] = led_attr.get<int>("pin_green");
					control->led_control.color_pin[BLUE] = led_attr.get<int>("pin_blue");
					control->led_control.alpha = led_attr.get<int>("alpha");
					control->led_control.color_led[RED] = get_internal_color(led_attr.get<int>("red"), control->led_control.alpha);
					control->led_control.color_led[GREEN] = get_internal_color(led_attr.get<int>("green"), control->led_control.alpha);
					control->led_control.color_led[BLUE] = get_internal_color(led_attr.get<int>("blue"), control->led_control.alpha);
					if (control->led_control.is_rgbw) control->led_control.color_led[WHITE] = get_internal_color(led_attr.get<int>("white"), control->led_control.alpha);
					control->led_control.effect_mode = led_attr.get<int>("mode");

					for (int i=RED; i < BLUE; i++) {
						const int pin_config = control->led_control.color_pin[i];
						control->pin_layout.enable_pin[pin_config] = true;
						control->pin_layout.pin_mode[pin_config] = LED;
					}
    } catch (exception &ex) {
    }

		try {
			BOOST_FOREACH (ptree::value_type &relai, root.get_child("relai_control")) {
				relai_control_t new_relai;
				new_relai.pin = relai.second.get<int>("pin");
				new_relai.state = relai.second.get<bool>("state");
				BOOST_FOREACH (ptree::value_type &member, relai.second.get_child("group_member")) {
					new_relai.group_member.push_back(member.second.get<string>("member"));
				}
			}		
		} catch (exception &ex) {

		}

    try {
			auto lcd_attr = root.get_child("lcd_control");
			control->lcd_control.enable = lcd_attr.get<bool>("enable");
			control->lcd_control.line_mode[0] = lcd_attr.get<int>("line1_mode");
			control->lcd_control.line_mode[1] = lcd_attr.get<int>("line2_mode");
			control->lcd_control.line_text[0] = lcd_attr.get<string>("line1_text");
			control->lcd_control.line_text[1] = lcd_attr.get<string>("line2_text");
			control->lcd_control.display_length = lcd_attr.get<size_t>("display_length");
			control->lcd_control.display_rows = lcd_attr.get<size_t>("display_rows");
			control->lcd_control.lcd_pins[0] = lcd_attr.get<int>("pin_vdd");
			control->lcd_control.lcd_pins[1] = lcd_attr.get<int>("pin_led");
			control->lcd_control.lcd_pins[2] = lcd_attr.get<int>("pin_rs");
			control->lcd_control.lcd_pins[3] = lcd_attr.get<int>("pin_e");
			control->lcd_control.lcd_pins[4] = lcd_attr.get<int>("pin_db0");
			control->lcd_control.lcd_pins[5] = lcd_attr.get<int>("pin_db1");
			control->lcd_control.lcd_pins[6] = lcd_attr.get<int>("pin_db2");
			control->lcd_control.lcd_pins[7] = lcd_attr.get<int>("pin_db3");
			control->lcd_control.lcd_pins[8] = lcd_attr.get<int>("pin_db4");
			control->lcd_control.lcd_pins[9] = lcd_attr.get<int>("pin_db5");
			control->lcd_control.lcd_pins[10] = lcd_attr.get<int>("pin_db6");
			control->lcd_control.lcd_pins[11] = lcd_attr.get<int>("pin_db7");

    } catch (exception &ex) {
        control->lcd_control.enable = false;
    }
    
    return CONFIG_SUCCESS;
}

int save_config(const general_control_t *control) {
    ptree root;
    ptree pin_layout_node;
    ptree led_config_node;
		ptree relai_config_node;
    ptree lcd_config_node;

    for (int i=0; i < PIN_NR; i++) {
        ptree pin_config_node;
        pin_config_node.put<int>("pin", i);
        pin_config_node.put<bool>("enable", control->pin_layout.enable_pin[i]);
        pin_config_node.put("mode", control->pin_layout.pin_mode[i]);

        pin_layout_node.push_back(make_pair("", pin_config_node));
    }
    root.add_child("pin_layout", pin_layout_node);

			led_config_node.put("enable", control->led_control.enable);
			led_config_node.put("is_rgbw", control->led_control.is_rgbw);
			led_config_node.put("pin_red", control->led_control.color_pin[RED]);
			led_config_node.put("pin_green", control->led_control.color_pin[GREEN]);
			led_config_node.put("pin_blue", control->led_control.color_pin[BLUE]);
			if (control->led_control.is_rgbw) led_config_node.put("pin_white", control->led_control.color_pin[WHITE]);
			led_config_node.put("alpha", control->led_control.alpha);
			led_config_node.put("red", get_external_color(control->led_control.color_led[RED], control->led_control.alpha));
			led_config_node.put("green", get_external_color(control->led_control.color_led[GREEN], control->led_control.alpha));
			led_config_node.put("blue", get_external_color(control->led_control.color_led[BLUE], control->led_control.alpha));
			if (control->led_control.is_rgbw) led_config_node.put("white", get_external_color(control->led_control.color_led[WHITE], control->led_control.alpha));
			led_config_node.put("mode", control->led_control.effect_mode);

    root.add_child("led_control", led_config_node);

		for (const auto &relai : control->relai_control_list) {
			ptree relai_node;
			relai_node.put<int>("pin", relai.second.pin);
			relai_node.put<bool>("state", relai.second.state);
			for (const string &member : relai.second.group_member) {
				ptree member_node;
				member_node.put<string>("member", member);
				relai_node.push_back(make_pair("", member_node));
			}
			relai_config_node.push_back(make_pair("", relai_node));
		}

	root.add_child("relai_control", relai_config_node);
			
    lcd_config_node.put("enable", control->lcd_control.enable);
    lcd_config_node.put("line1_mode", control->lcd_control.line_mode[0]);
    lcd_config_node.put("line2_mode", control->lcd_control.line_mode[1]);
    lcd_config_node.put("line1_text", control->lcd_control.line_text[0]);
    lcd_config_node.put("line2_text", control->lcd_control.line_text[1]);
    lcd_config_node.put("display_length", control->lcd_control.display_length);
    lcd_config_node.put("display_rows", control->lcd_control.display_rows);
    lcd_config_node.put("pin_vdd", control->lcd_control.lcd_pins[VDD]);
    lcd_config_node.put("pin_led", control->lcd_control.lcd_pins[LED_POSITIVE]);
    lcd_config_node.put("pin_rs", control->lcd_control.lcd_pins[RS]);
    lcd_config_node.put("pin_e", control->lcd_control.lcd_pins[E]);
    lcd_config_node.put("pin_db0", control->lcd_control.lcd_pins[DB0]);
    lcd_config_node.put("pin_db1", control->lcd_control.lcd_pins[DB1]);
    lcd_config_node.put("pin_db2", control->lcd_control.lcd_pins[DB2]);
    lcd_config_node.put("pin_db3", control->lcd_control.lcd_pins[DB3]);
    lcd_config_node.put("pin_db4", control->lcd_control.lcd_pins[DB4]);
    lcd_config_node.put("pin_db5", control->lcd_control.lcd_pins[DB5]);
    lcd_config_node.put("pin_db6", control->lcd_control.lcd_pins[DB6]);
    lcd_config_node.put("pin_db7", control->lcd_control.lcd_pins[DB7]);
		
    root.add_child("lcd_control", lcd_config_node);

    try {
        write_json(CONFIG_FILE, root); 
    } catch (boost::property_tree::json_parser::json_parser_error &er) {
        return WRITE_FAIL;
    }
    
    return CONFIG_SUCCESS;
}
