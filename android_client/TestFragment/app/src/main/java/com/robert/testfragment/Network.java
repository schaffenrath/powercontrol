package com.robert.testfragment;

import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class Network {
    NotifyManager notifyManager;
    Socket socket;
    OutputStream out;
    InputStream in;
    byte[] inputBuffer = new byte[32];
    boolean connectionState = false;

    final int PORT = 65234;
    final String IP = "10.0.0.3";
    public final static String SHUTDOWN_SIGNAL           = "0";
    public final static String DISCONNECT_SIGNAL         = "1";
    public final static String VERIFY_CONNECTION         = "2";
    public final static String REQUEST_PIN               = "10";
    public final static String REQUEST_LED_STATE         = "11";
    public final static String REQUEST_LED_COLOR         = "12";
    public final static String COMMAND_LED_STATE         = "101";
    public final static String COMMAND_LED_COLOR         = "102";
    public final static String COMMAND_CREATE_RELAI       = "104";
    public final static String COMMAND_CREATE_RELAI_GROUP = "105";
    public final static String COMMAND_RELAI_STATE        = "106";
    public final static String COMMAND_REMOVE_RELAI       = "108";
    public final static String COMMAND_REMOVE_RELAI_GROUP = "109";
    public final static String COMMAND_LCD_TEXT          = "111";

    public static String VALUE_DELIMITER        = ":";
    public static String COMMAND_DELIMITER      = "\t";
    public static String MESSAGE_TERMINATOR     = "\n";

    public Network() {}
    public Network(NotifyManager notifyManager) {
        this.notifyManager = notifyManager;
    }


    public boolean connect() {
        try{
            socket = new Socket(IP, PORT);
            in = socket.getInputStream();
            out = socket.getOutputStream();
            connectionState = true;
            return true;
        } catch (UnknownHostException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
    }

    public boolean checkConnection() {

        try {
            if (socket == null) {
                connectionState = false;
                return false;
            }

            out.write((VERIFY_CONNECTION + MESSAGE_TERMINATOR).getBytes(StandardCharsets.US_ASCII));
            out.flush();
            in.read(inputBuffer);
        } catch (IOException e) {
            connectionState = false;
            return false;
        }
        connectionState = true;
        return true;
    }

    public boolean connectionState() {
        return connectionState;
    }

    public int sendMessage(String... message) {
        try {
                if (!connectionState) {
                    notifyManager.createNotification("Network error", "Not connected to Raspberry Pi");
                    return -1;
                }

                StringBuilder messageCommand = new StringBuilder();
                boolean firstCommand = true;
                for (String arg : message) {
                    messageCommand.append(arg);
                    if (firstCommand) {
                        messageCommand.append(Network.COMMAND_DELIMITER);
                        firstCommand = false;
                    }
                    else
                        messageCommand.append(Network.VALUE_DELIMITER);
                }
            out.write((messageCommand.toString() + MESSAGE_TERMINATOR).getBytes(StandardCharsets.US_ASCII));
            out.flush();
            AtomicReference<String> receivedMessage = new AtomicReference<>("");
            AtomicBoolean doneReading = new AtomicBoolean(false);

            while(!doneReading.get()) {
                in.read(inputBuffer);
                String answer = new String(inputBuffer, StandardCharsets.US_ASCII);
                if (answer.contains(Network.MESSAGE_TERMINATOR)) {
                    doneReading.set(true);
                    answer = answer.substring(0, answer.indexOf(Network.MESSAGE_TERMINATOR));
                }
                receivedMessage.set(receivedMessage + answer);
            }
            return Integer.valueOf(receivedMessage.get());
        } catch (IOException e) {
            if (!connect())
                return -1;
            else
                return sendMessage(message);
        }
    }

    public String sendRequestMessage(String message) {
        try {
            if (socket == null || socket.getInetAddress().isReachable(100)) {
                if (!connect()) {
                    return "Failed to send message!";
                }
            }

            out.write((message + MESSAGE_TERMINATOR).getBytes(StandardCharsets.US_ASCII));
            out.flush();
            AtomicReference<String> receivedMessage = new AtomicReference<>("");
            AtomicBoolean doneReading = new AtomicBoolean(false);

            while(!doneReading.get()) {
                in.read(inputBuffer);
                String answer = new String(inputBuffer, StandardCharsets.US_ASCII);
                if (answer.contains(Network.MESSAGE_TERMINATOR)) {
                    doneReading.set(true);
                    answer = answer.substring(0, answer.indexOf(Network.MESSAGE_TERMINATOR));
                }
                receivedMessage.set(receivedMessage + answer);
            }
            return receivedMessage.get();
        } catch (IOException e) {
            if (!connect())
                return "Failed to send message!";
            else
                return sendRequestMessage(message);
        }
    }
}
