package com.robert.testfragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.divyanshu.colorseekbar.ColorSeekBar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Overview.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Overview#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Overview extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private View view;
    private Network network;
    private NotifyManager notifyManager;
    private LedColor ledColor;
    private ColorSeekBar colorSeekBar;
    private Toolbar toolbar;
    private MenuItem connectionIndicator;
    boolean fragmentActive = false;

    int buttonState = 0;
    public Overview() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Overview.
     */
    // TODO: Rename and change types and number of parameters
    public static Overview newInstance(String param1, String param2) {
        Overview fragment = new Overview();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        network = ((MainActivity)getActivity()).getNetwork();
        notifyManager = ((MainActivity)getActivity()).getNotifyManager();
        ledColor = new LedColor();
        toolbar = ((MainActivity)getActivity()).getToolbar();
        connectionIndicator = toolbar.getMenu().findItem(R.id.connection_indicator);


        view = inflater.inflate(R.layout.fragment_main, container, false);

        colorSeekBar = view.findViewById(R.id.color_seekBar);
        colorSeekBar.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @Override
            public void onColorChangeListener(int i) {
                //Android Color i with shift operations to RGBA
                ledColor.setColor( (i >> 16) & 0xff,(i >> 8) & 0xff,(i) & 0xff);
                network.sendMessage(ledColor.getChangeColorNetCommand());
            }
        });

        final ImageView imageView = view.findViewById(R.id.ledIndicator);
        Button ledSwitchButton = view.findViewById(R.id.buttonLedSwitch);
        ledSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                final int response = network.sendMessage(Network.COMMAND_LED_STATE, ((buttonState == 0)?"1":"0"));

                if (response == 1) {
                    buttonState = 1;
                        imageView.setImageResource(R.drawable.ic_led_on);
                        ((Button) view).setText("On");
                    }
                    else {
                        buttonState = 0;
                        imageView.setImageResource(R.drawable.ic_led_off);
                        ((Button) view).setText("Off");
                }
            }
        });

        Button createRelaiButton = view.findViewById(R.id.createRelai);
        createRelaiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                network.sendMessage(Network.COMMAND_CREATE_RELAI, "One", "26");
            }
        });
        SeekBar brightnessBar = view.findViewById(R.id.brightnessSeekBar);
        brightnessBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                ledColor.setAlpha(i);
                network.sendMessage(ledColor.getChangeColorNetCommand());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        fragmentActive = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                int setIcon = 0;
                try {
                    Thread.sleep(1000);
                    while(fragmentActive) {
                        Thread.sleep(2000);
                        if (network.checkConnection()) {
                            //don't change if already changed
                            if (setIcon != 1) {
                                setIcon = 1;
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        toolbar.getMenu().findItem(R.id.connection_indicator).setIcon(R.drawable.ic_connected);
                                    }
                                });
                                int delimiterPos = 0;
                                int[] colorValue = new int [4];
                                String colorMessage = network.sendRequestMessage(Network.REQUEST_LED_COLOR);
                                for (int i=0; i < 4; i++) {
                                    delimiterPos = colorMessage.indexOf((i!=3) ? Network.VALUE_DELIMITER : Network.COMMAND_DELIMITER);
                                    colorValue[i] = Integer.valueOf(colorMessage.substring(0, delimiterPos));
                                    colorMessage = colorMessage.substring(delimiterPos+1);
                                }

                                ledColor.setColor(colorValue[0], colorValue[1], colorValue[2], colorValue[3]);
                                if (network.sendRequestMessage(Network.REQUEST_LED_STATE).equals("1")) {
                                    buttonState = 1;
                                    view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            ImageView ledIndicator = view.findViewById(R.id.ledIndicator);
                                            ledIndicator.setImageResource(R.drawable.ic_led_on);
                                            ((Button)view.findViewById(R.id.buttonLedSwitch)).setText("On");
//                                            Button buttonLedColor = view.findViewById(R.id.buttonLedColor);
//                                            buttonLedColor.setBackgroundColor(Color.argb(ledColor.getAlpha(), ledColor.getRed(), ledColor.getGreen(), ledColor.getBlue()));
                                        }
                                    });
                                } else {
                                    buttonState = 0;
                                    view.post(new Runnable() {
                                        @Override
                                        public void run() {
                                            ImageView ledIndicator = view.findViewById(R.id.ledIndicator);
                                            ledIndicator.setImageResource(R.drawable.ic_led_off);
                                            Button buttonLedState = view.findViewById(R.id.buttonLedSwitch);
                                            buttonLedState.setText("Off");
//                                            Button buttonLedColor = view.findViewById(R.id.buttonLedColor);
//                                            buttonLedColor.setBackgroundColor(Color.argb(ledColor.getAlpha(), ledColor.getRed(), ledColor.getGreen(), ledColor.getBlue()));
                                        }
                                    });

                                }

                            }
                        } else {
                            //don't change if already changed
                            if (setIcon != 2) {
                                setIcon = 2;
                                view.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        toolbar.getMenu().findItem(R.id.connection_indicator).setIcon(R.drawable.ic_disconnected);
                                    }
                                });
                            }
                        }
                    }
                } catch (InterruptedException e) {
                    Log.e("tread", "thread got interrupted");
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void onPause() {
        super.onPause();
        fragmentActive = false;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("fragment is currently detached", "something");
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
