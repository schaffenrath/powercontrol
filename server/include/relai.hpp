#pragma once
#include "control.hpp"
#include <wiringPi.h>

bool create_relai(general_control_t *control, const string &name, const int pin);
bool create_relai_group(general_control_t *control, const string &group_name, vector<string> &relai_names);
void remove_relai(general_control_t *control, const string &name);
void remove_relai(general_control_t *control, const string &group_name);
bool switch_relai(general_control_t *control, const string &group, const bool state);
void control_relai(general_control_t *control);
