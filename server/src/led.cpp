#include "../include/led.hpp"
#include <condition_variable>
#include <iostream>
#include <mutex>

float get_internal_color(int color_value, int alpha) {
  return (float)(color_value / 255.0f) * alpha;
}

int get_external_color(float color_value, int alpha) {
  return (int) ((color_value / alpha) * 255);
}

bool switch_led(general_control_t *control, const bool state) {
		control->led_control.enable = state;
		control->led_enable_var.notify_all();
		return true;
}

bool control_led(general_control_t *control) {
#ifdef __arm__
	for (size_t i=1 ; i < RGB_LED_NR+1; i++)
    softPwmCreate(control->led_control.color_pin[i], 0, 100);

  unique_lock<mutex> disabled_lock(control->mtx);

  while (!control->shutdown) {
   
      if (!control->led_control.enable) {
				for (size_t i=1; i < RGB_LED_NR+1; i++)
					softPwmWrite(control->led_control.color_pin[i], 0);
      } 
			else {
				for (size_t i=1; i < RGB_LED_NR+1; i++)  {
        softPwmWrite(control->led_control.color_pin[i],
                     control->led_control.color_led[i]);
				}
      }

    while (!control->led_control.enable) 
      control->led_enable_var.wait(disabled_lock);
      
    delay(10);
		}
		for (size_t i=1; i < RGB_LED_NR+1; i++)
			softPwmWrite(control->led_control.color_pin[i], 0);
#endif
  return true;
}
